<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3</title>
        <style type="text/css">
            #layer1{
                position: absolute;
                left: 62px;
                top: 38px;
                width: 264px;
                height: 170px;
                z-index: 1;
            }
            #layer2{
                position: absolute;
                left: 535px;
                top:139px;
                width: 498px;
                height: 142px;
                z-index: 2;
                background-color: #FFFFCC;
            } 
            
            
            
        </style>
    </head>
    <body>
        <?php
            $numero1=10;
            $numero2=3;
        ?>
        <div id="layer1">
            <h2>Los numeros utilizados son:</h2>
            <h3><p><?=$numero1;?></p></h3>
            <h3><p><?=$numero2;?></p></h3>
            
        </div>
        <div id="layer2">
            <table width="100%" height="137px">
                <tbody>
                    <tr>
                        <td width="41%">Suma</td>
                        <td width="59%"><?=$numero1+$numero2;?></td>
                    </tr>
                    <tr>
                        <td width="41%">Resta</td>
                        <td width="59%"><?=$numero1-$numero2;?></td>
                    </tr>
                    <tr>
                        <td width="41%">Producto</td>
                        <td width="59%"><?=$numero1*$numero2;?></td>
                    </tr>
                    <tr>
                        <td width="41%">Division</td>
                        <td width="59%"><?=$numero1/$numero2;?></td>
                    </tr>
                    <tr>
                        <td width="41%">Resto</td>
                        <td width="59%"><?=$numero1%$numero2;?></td>
                    </tr>
                </tbody>
            </table>

            
        </div>
    </body>
</html>
