<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 7 y 8</title>
        <style type="text/css">
            #layer1{
                position: absolute;
                left: 90px;
                top: 70px;
                width: 323px;
                height: 504px;
                z-index: 1;
                padding: 10px;
                border: medium solid #0000FF;
            }
             #layer2{
                position: absolute;
                left: 519px;
                top: 90px;
                width: 503px;
                height: 266px;
                z-index: 2;
              
            }
        </style>
    </head>
    <body>
        <?php
            $dias_semana=['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];
            $colores=[
                'uno'=>'rojo',
                'dos'=>'verde',
                'tres'=>'azul'
                ];
        ?>
        
        
        <div id="layer1">
            <h3>Los días de la semana son:</h3>
            <?php
                  for($i=0;$i<count($dias_semana);$i++){
                      
                      echo "<div>$dias_semana[$i]</div>";
                  }               
            ?>
            
            
        </div>
        <div id="layer2">
            <h3>Los colores son:</h3>
            <?php
                foreach($colores as $color){
            ?>        
            <div><?=$color;?></div>
            <?php
                }
            ?>
            
        </div>
            
        
    </body>
</html>
